SeedMe Python Client/Module
===============================================================================
SeedMe module provides convenient utilities to interact with [web services at SeedMe.org](https://www.seedme.org/help). This module provides methods for programmatic access as well as command line interface. It performs extensive input data sanity checks to
speed up integration, interaction and testing without implementing your own REST client for SeedMe.org. Usage information about this module is provided below 

###A. [Files Provided](#markdown-header-a-files-provided_1)
###B. [Requirements](#markdown-header-b-requirements_1)
###C. [System and Terminology](#markdown-header-c-terminology_1)
###D. [Module Usage](#markdown-header-e-module-usage)
###E. [Command Line Usage](#markdown-header-f-command-line-usage)
###D. [Questions and Feedback](#markdown-header-g-questions-and-feedback)

- - -

A. Files Provided
-------------------------------------------------------------------------------
- [demo.py](https://bitbucket.org/seedme/seedme-python-client/src/master/demo.py?at=master): Demonstration of using the SeedMe client/module. You must [download sample files](https://www.seedme.org/sites/seedme.org/files/downloads/sample.zip) to make use of demo.py. The sample files contains files, plots, sequences and videos.
- [LICENSE.txt](https://bitbucket.org/seedme/seedme-python-client/src/master/LICENSE.txt?at=master): License information for this module
- [NOTICE.txt](https://bitbucket.org/seedme/seedme-python-client/src/master/NOTICE.txt?at=master): Combined license information for thirdparty modules
- [seedme.html](https://bitbucket.org/seedme/seedme-python-client/src/master/seedme.html?at=master): Documentation of methods available in SeedMe Module
- [seedme.py](https://bitbucket.org/seedme/seedme-python-client/src/master/seedme.py?at=master): SeedMe module and command line utility
- [setup.py](https://bitbucket.org/seedme/seedme-python-client/src/master/setup.py?at=master): Set up file for installing SeedMe module 
- [thirdparty](https://bitbucket.org/seedme/seedme-python-client/src/master/thirdparty?at=master): This folder contains third-party modules listed below, they are only used when not found on user's system
    - thirdparty/argeparse.py : argparse module for Python 2.6
    - thirdparty/ez_setup.py : ez_setup module for aiding installation 
    - thirdparty/requests : requests module if not available in user's Python
    - thirdparty/simplejson : json module for Python2.6 

[Top](#markdown-header-seedme-python-clientmodule)

- - -

B. Requirements
-------------------------------------------------------------------------------
- **Account at SeedMe.org**
- Python 2.6 (works, but not well tested)
- **Python 2.7 (Recommended)**
- Python 3.x (works, but not well tested)
- Curl executable (Optional): By default this module uses python requests to upload content, however it could use curl executable in user's environment path or a specified path.
- [Download sample files](https://www.seedme.org/sites/seedme.org/files/downloads/sample.zip) containing files, plots, sequences and videos. Unzip and move this folder inside the module folder

[Top](#markdown-header-seedme-python-clientmodule)

- - -

C. System and Terminology
-------------------------------------------------------------------------------
SeedMe is a platform to share data. [See more information here](https://www.seedme.org/help/intro)

**Collection** is a container for that may have following elements

- **collection_id:** Each collection is automatically assigned a unique numeric identifier
- **Metadata:** Title, Description, Key-Value Pairs for the collection
- **Files:** These could be files, images, videos
- **Sequences:** Set of related images 
- **Tickers:** Short text string (128 chars) useful for monitoring progress
- **File size:** Each file must be less than 100 MB 

[Top](#markdown-header-seedme-python-clientmodule)

- - -

D. Module Usage
-------------------------------------------------------------------------------
**Who should use this method?** Power users and application integrators  

1. [**Download latest SeedMe module**](https://bitbucket.org/seedme/seedme-python-client/get/master.zip)  

2. **[Signup](https://www.seedme.org/user/register) at seedme.org** 
    Fetch your authorization credentials: username and apikey
    
    **Note:** apikey is different than password  

3. **Download your authorization file**
    [Download web services authorization file](https://www.seedme.org/user) from your account. After downloading move this file to your home folder

    | Operating System                    | Home Folder Location                 |
    |-------------------------------------|--------------------------------------|
    | Microsoft Windows 2000, XP and 2003 | C:\Documents and Settings\<username> |
    | Microsoft Windows Vista, 7, 8, 10   | C:\Users\<username>                  |
    | Mac OS X                            | /Users/<username>                    |
    | Unix-Based | Varies, check in terminal as follows  `% echo $HOME` OR `% cd ~ ; pwd`|

    The authorization file is a text file in json format shown below

    `{ "username" : "YourUserName", "apikey" : "YourApiKey" }`

    **Linux and Mac Power Users**

    * Restrict the file read privileges to yourself by issuing the following commands on the terminal `% chmod 600 seedme.txt`
    * Rename "seedme.txt" file to ".seedme" and move it to your home folder


4. **View available methods**
    See available methods in **seedme.html** or by running

    `% pydoc seedme`

5. **[Download sample files](https://www.seedme.org/sites/seedme.org/files/downloads/sample.zip)** containing files, plots, sequences and videos. Unzip and move this folder inside the module folder for use with demo.py

6. **Review code provided in demo.py**

    The [demo.py](https://bitbucket.org/seedme/seedme-python-client/src/master/demo.py?at=master) file  demonstrates how to use this module programmatically. To run the demo.py execute the following

    `% python demo.py`

[Top](#markdown-header-seedme-python-clientmodule)

- - -

E. Command Line Usage
-------------------------------------------------------------------------------
See help as follows
    `% python seedme.py -h`

For step by step instruction see [using the command-line to access SeedMe](https://www.seedme.org/help/use/command-line) 

[Top](#markdown-header-seedme-python-clientmodule)

- - -

F. Contact
-------------------------------------------------------------------------------
You may contact us via

- Email us:   help AT seedme.org
- [Web form](http://www.seedme.org/contact)
- Open an [issue](https://bitbucket.org/seedme/seedme-python-client/issues) 
- Post and subscribe to [SeedMe Users mail list]([http://www.seedme.org/contact](http://www.seedme.org/contact)

[Top](#markdown-header-seedme-python-clientmodule)
