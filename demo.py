#!/usr/bin/env python
"""demo.py: Demonstration of seedme module.
Read this file top to bottom

SeedMe Collections are a container of any of the following elements
- collection_id: Each collection is automatically assigned numeric identifier
- PRIVACY: Collections can be either public, group or private(default).
- SHARING: Add emails to share a collection.
- NOTIFICATION: When share emails are provided an email is automatically sent, it could also be triggered again later
- METADATA: Title, Description, Key Value Pairs
- TICKER: Short text string (128 chars) useful for monitoring progress
- SEQUENCES: Set of related images
- FILES: unrelated files, images, videos

View supported filetypes and size limit information.
These limitations are anticipated to be relaxed over time
https://www.seedme.org/documentation/filetypes

Create and update a collection comprising of above elements by using
methods provided in the module to interact with SeedMe web services.

##############################################################################
# NOTE: create_collection, update_collection, query and download
# are most important as they can perform all web services interactions
#
# get_id, get_message and get_url are useful to capture results
#
# The following methods are wrapper around update_collection method.
# These enable use of simple arguments instead of creating list or dict
#       update_descriptions, update_title
#       add_file, add_sequence, encode_sequence
##############################################################################

For command line usage see README.md or visit
https://www.seedme.org/help/use/command-line
"""

import seedme # import the seedme module
import random # not required for SeedMe, just used here in this demo
import time # not required for SeedMe, just used here in this demo
import os

# Silence InsecurePlatformWarning for some Python versions prior to 2.7.9
# https://urllib3.readthedocs.org/en/latest/security.html#insecureplatformwarning
# import logging
# logging.captureWarnings(True)


# Create an object from SeedMe class
obj = seedme.SeedMe()
obj.set_base_url("https://www.seedme.org/services/1.1/collection1_1")


##############################################################################
# Set Authorization [Optional when credentials stored at default location ]
##############################################################################
# Test whether default authoization file exists
# seedme.test_default_auth() # returns boolean

# Set authorization using a file (preferred method)
# Download your authorization file seedme.txt and place it in your
# home folder. This file must contain the following
# { "username" : "YourUserName",    "apikey" : "YourApiKey" }
# Hide this file from prying eyes % chmod 600 seedme.txt
#
# If this file is placed elsewhere use the following method to set auth path
# obj.set_auth_via_file('/path-to/seedme.txt')
obj.set_auth_via_file('~/seedme.txt') # This not needed as its default



##############################################################################
# Initialize variables we will use later
##############################################################################
# Collection privacy
my_privacy = 'public' # private (default), public, group

# Collection share list
# space OR comma separated emails in a string
# this setting just adds users for sharing but does not notify automatically
# trigger notification by setting notify = True or using notify() method
my_sharing = "one@example.com, two@example.com"


# Collection Meta Data
my_title = 'Original collection title' #string

my_description = 'Many entities may be uploaded to SeedMe' #string

my_tags = 'sdsc' #string



##############################################################################
# Create a new collection using create_collection method
# comprising of title, description and tags
# with public access, shared with two people
# ARGUMENTS
# my_cid, #REQUIRED string OR int
# description='modified description', #string
# privacy='group', #string
# sharing=my_sharing, #string OR space delimited string
# title='Fully populated collection items', #string
# keyvalues=my_keyvalues, # One colon delimited string OR dict
# files=my_files, #dict OR list of dict
# notify=True, #send email notification to shared users
# sequences=my_sequences, #dict OR list of dict
# tickers=my_tickers, #string OR list of strings
# transfer=my_recipient, # string with single email
##############################################################################
print('Using create_collection method to create a new collection')
result = obj.create_collection(title=my_title,
                               description=my_description,
                               privacy=my_privacy,
                               sharing=my_sharing,
                               tags=my_tags,
                              )


##############################################################################
# get collection_id for updating this collection
##############################################################################
# create_collection returns the result as a string in json format
my_cid = obj.get_id(result)
if not my_cid:
    print("Abort: Collection id not found in: " + result)
    exit(1)


##############################################################################
# We can get the url for the new collection using get_url method
##############################################################################
url = obj.get_url(result)
print("Collection url is: " + url)


##############################################################################
# Modify collection privacy and META DATA: title, description,
# using update_collection
# ARGUMENTS
# my_cid, #REQUIRED string OR int
# description='modified description', #string
# privacy='group', #string
# sharing=my_sharing, #string OR space delimited string
# title='Fully populated collection items', #string
# keyvalues=my_keyvalues, # One colon delimited string OR dict
# files=my_files, #dict OR list of dict
# notify=True, #send email notification to shared users
# sequences=my_sequences, #dict OR list of dict
# tickers=my_tickers, #string OR list of strings
# transfer=my_recipient, # string with single email
##############################################################################
print("\nModify privacy and meta data using update_collection method")
obj.update_collection(my_cid,
                      description='modified description',
                      privacy='group',
                      sharing=my_sharing,
                      title='Fully populated collection items',
                     )

# Another way to modify Meta Data is by using the following methods
# obj.update_description(my_cid, 'latest description')
# obj.update_privacy(my_cid, 'group')
# obj.update_title(my_cid, 'latest title')


##############################################################################
# Add KEY VALUE pairs by updating this newly created collection
##############################################################################
# Create a dictionary of key value pairs which we wish to add
my_keyvalues = {"Viscosity":"10 Kpa",
                "Pressure":"1.0mpa"}

print("\n\nAdding new KEY VALUE pairs using update_collection method")
obj.update_collection(my_cid, keyvalues=my_keyvalues)

# Another way to add KEY VALUE pairs is by using add_keyvalue method
# Note: one key value pair may be passed as a string with colon delimeter
print('\n\nAlternatively, add new KEY VALUE via add_keyvalue method')
obj.add_keyvalue(my_cid, "Density:33.33") # add one key value pair as string

# Note: multiple key value pairs must be passed as a dictionary
new_pairs = {'Temperature':'300K', 'Velocity':'30m/s'}
obj.add_keyvalue(my_cid, new_pairs) # add multiple kv pairs



##############################################################################
# Add TICKER by updating this collection
##############################################################################
# Create a list of strings which we wish to add as tickers
my_tickers = ['step 10: convergence: 0%',
              'step 30: convergence 25%',
              'step 50: convergence 39%',
              'step 70: convergence 50%',
             ] #string array

print("\n\nAdding new TICKERS using update_collection method")
for item in my_tickers:
    print('Adding ticker - "' + item + '"')
    # Add one TICKER text at a time to an existing collection
    obj.update_collection(my_cid, tickers=item)
    time.sleep(random.random()*3 + 1) #sleep for short time to emulate progress

# Another way to add new TICKERS is by using add_ticker method
print('\n\nAlternatively, add new TICKER via add_ticker method')
obj.add_ticker(my_cid, 'step 80: convergence 70%') # add one ticker as a string

#new_tickers = ['step 90: convergence 80%', 'step 95: convergence 100%']
#obj.add_ticker(my_cid, new_tickers) # add multiple tickers via list of strings
# Note: Be careful with sending multiple tickers at once, as they will be
# marked with same timestamp and will not display correctly in intended order



##############################################################################
# Add FILES to this Collection
# Single or Multiple files
##############################################################################
# Add single file as a simple dict structure
# filepath: multiple files can be specified using the * wildcard

'''
my_files = {'filepath': 'sample/files/how_it_works.pdf',
            'title': 'How SeedMe Works',
            'description': 'Schematic description of how to use',
           }

'''

# Add two or more specific files as a list containing two dictionaries
my_files = [
             # add image
             {'filepath': 'sample/plots/node.png', #required
              'title': 'Node of villi',
              'description': 'Rendering of node',
             },

             # add file
             {'filepath': 'sample/files/progress_bars.ipynb', #required
              'title': 'Running Code in the IPython Notebook',
              'description': 'IPython Notebook is an interactive environment.',
             },

             # add video
             {'filepath': 'sample/videos/quake.mp4', #required
              'title': 'Simulation of a quake',
              'description': 'Visualization of magnitude 7.7 earthquake',
             }
           ]

print("\n\nAdd new FILES using update_collection method")
obj.update_collection(my_cid, files=my_files)


# Another way to add new FILE is by using add_file method
print('\n\nAlternatively, add new FILE via add_file method')
# Note: all arguments passed here are strings, etc not dict
# filepath: multiple files can be specified using the * wildcard
obj.add_file(my_cid, #required
             filepath='sample/videos/quake.mp4', #required
             title='Simulation of a quake',
             description='Visualization of magnitude 7.7 earthquake.',
             fps=1.4,
             #overwrite=True
            )


##############################################################################
# Add IMAGE SEQUENCES to Collection
# Single or Multiple sequences using * wild card
##############################################################################

# Add multiple sequences as a list containing dictionaries
# filepath is specified using wild card * or dir or list of files
my_sequences = [
                 {'filepath': 'sample/sequences/mandelbrot_small/*', #required
                  'title': 'Mandelbrot small seq using wild card *',
                  'description': 'Pattern by solving Mandelbrot equation',
                  'fps': '2', #set frame rate for video
                  'encode': True, #create video from this sequence
                 },
                 {'filepath': 'sample/sequences/vx/cpu_x.000*', #required
                  'title': 'Quake horizontal velocity using wild card *',
                  'description': 'Visualization of Quake',
                 },

                 {'filepath': ['sample/sequences/vx/cpu_x.0001100.png',
                               'sample/sequences/vx/cpu_x.0001200.png',
                               'sample/sequences/vx/cpu_x.0001300.png',], # list of files
                  'title': 'Sequence with a list of files',
                 }
               ]

print("\n\nAdd new SEQUENCES using update_collection method")
obj.update_collection(my_cid, sequences=my_sequences)

# Upload another IMAGE SEQUENCE using add_sequence method
print('\n\nAlternatively, add SEQUENCE via add_seqeunce method')
# Note: all arguments passed here are strings, not dict
obj.add_sequence(my_cid, #required
                 filepath='sample/sequences/mandelbrot/*jpg', #required
                 title='Mandelbrot using wild card *',
                 description='Pattern created by solving Mandelbrot equation',
                )


# Note: You may append files to existing sequences
# using update_collection or add_sequence methods


# After finishing appends create a video from above IMAGE SEQUENCE
# at 10 frames per second using encode_sequence method
# Default: frames per second is 30
print('\n\nUse encode_sequence method to create video from above sequence')
# Note: all arguments passed here are strings, not dict
obj.encode_sequence(my_cid, #required
                    #Title should be identical to existing one (like above)
                    title='Mandelbrot using wild card *', #required
                    fps='10',
                   )


##############################################################################
# Notify shared users for a given collection
##############################################################################
# Now that we have something in the collection we could
# send an email notification to shared users
obj.notify(my_cid)


##############################################################################
# Add TAGS by updating this collection
# Application integrators could add their app tag by default during create
##############################################################################
# Create a string  OR list of strings that we wish to add
one_tag = 'SeedMe' #string
many_tags = ['SeedMe', 'Visualization'] #list of strings

print ("\n\nAdding TAGS using update_collection method")
obj.update_collection(my_cid, tags=many_tags)

# Another way to add new TAGS is by using add_tag method
print ("\n\nAlternatively, add new TAGS via add_tag method")
obj.add_tag(my_cid, 'SDSC') # add one tag as a string
new_tags = ['climate', 'ionosphere']
obj.add_tag(my_cid, new_tags) # add multiple tag as a list of strings


##############################################################################
# Delete SeedMe Object
##############################################################################
# The object stores state of customizations we have made
# So delete it once your operations are complete and start afresh
del obj # delete seedme object: obj


##############################################################################
# Query All Collections
##############################################################################
# Create an object from SeedMe class
obj = seedme.SeedMe()

# Fetch list of all your collections
print("\n\nFetching a list of all your collections")
obj.query()

# add tail to list last 5 collections
#obj.query(tail=5)


# Fetch list of all collections that match all specified key values
print("\n\nFind my collections that match key value pair = 'Pressure:1.0mpa'")
obj.query(keyvalues="Pressure:1.0mpa")

# add tail to list last 5 collections
#obj.query(keyvalues="Pressure:1.0mpa", tail=5)


##############################################################################
# Query Single Collections
##############################################################################
# List contents for a specified collection
print("\n\nRetrive and list all content for collecion id = " + str(my_cid))
obj.query(cid=my_cid, content='all')

#List files and URLs for a specified collection
# Note: Urls are only listed for public collections
print("\n\nRetrive files for collecion id = " + str(my_cid))
obj.query(cid=my_cid, content='url')

# Now lets change the permission for this collection to public and try again
print("\n\nMaking collection id " + str(my_cid) + " public")
obj.update_collection(my_cid, privacy='public')

# Now try to retrieve files
print("\n\nRetrive files from now public collecion id = " + str(my_cid))
obj.query(cid=my_cid, content='url')

# add tail to list last 5 files
#obj.query(cid=my_cid, content='url', tail=5)


# List last 5 Tickers for a specified collection
print("\n\nRetrive last 5 Tickers for collecion id = " + str(my_cid))
obj.query(cid=my_cid, content='tic', tail=5)

del obj # delete seedme object: obj


##############################################################################
# Download files from a collection
##############################################################################
# Lets create a dir where we will download the files
download_path = os.path.normpath(os.path.expanduser('~/Downloads/' + str(my_cid)))

if not os.path.exists(download_path):
    os.makedirs(download_path)

# Create an object from SeedMe class
obj = seedme.SeedMe()

# Download all files from a specified collection
print("\n\nDownload all files from collection ID "  + str(my_cid))
obj.download(my_cid, content='all', path=download_path)

# Download only video files from a specified collection
print("\n\nDownload only video files from collection ID "  + str(my_cid))
obj.download(my_cid, content='video', path=download_path)

# Download files that match a wildcard pattern from a specified collection
print("\n\nDownload files that match '*png' pattern from collection ID "  + str(my_cid))
obj.download(my_cid, content='*png', path=download_path)

del obj # delete seedme object: obj


##############################################################################
# Delete collection and all its content
##############################################################################
'''
# Create an object from SeedMe class
obj = seedme.SeedMe()

# delete a collection with id 1234 and all its content
obj.delete(1234)

# delete seedme object: obj
del obj
'''

##############################################################################
# Delete files from a collection
##############################################################################
'''
# Create an object from SeedMe class
obj = seedme.SeedMe()

# delete doc.pdf file from collection with id 1234
obj.delete(1234, "doc.pdf")

# delete all png files from collection with id 1234
obj.delete(1234, "*png")

del obj # delete seedme object: obj
'''




##############################################################################
#
# ADVANCED USAGE
#
##############################################################################


##############################################################################
# Transfer collection ownership
##############################################################################
'''
# Create an object from SeedMe class
obj = seedme.SeedMe()

# Transfer a collection during creation
obj.create_collection(title="Test title", transfer="one@example.com")

# Transfer an existing collection ID 1234 during update
obj.update_collection(1234, transfer="one@example.com")

Notes: Transferee may still update the transferred collection after transfer
       Privacy is restricted to private or group
       Cannot delete collection or its content after transfer
       Cannot rescind transfer
       Cannot send notifications to shared users

del obj # delete seedme object: obj
'''


##############################################################################
# Sets path to SSL certificate file to be used for secure communication.
# This is only needed in special cases when
#    * default certifcate cacert.pem is not found in default system path
#    * default cacert.pem certifacate does not work in firewalled zones
#    * REQUESTS_CA_BUNDLE environment variable to cert path is not set
##############################################################################
# obj.set_ssl_cert('path/to/cacert.pem')



##############################################################################
# Overide base URL for webservices
##############################################################################
# Only useful for interacting with other version of webservices API
# obj.set_base_url('https://www.seedme.org/services/1.1/collection1_1')

# Logging
##############################################################################
# Output logs to a file
# obj.set_file_log(True, 'log')

# Turn off console output including errors
# obj.set_console_log(False)
